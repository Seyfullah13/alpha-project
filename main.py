def somme(*args):
    result = 0
    for arg in args: 
        result += arg
    return result

print(somme(2, 3, 4))
